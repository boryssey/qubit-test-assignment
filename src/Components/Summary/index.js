import React, {useEffect} from 'react'
import {connect} from 'react-redux'
import {useHistory} from  "react-router-dom"
import './styles/index.css'
const Summary = (props) =>{
  console.log(props.families)
  const history = useHistory();


  useEffect(() => {
    if(!props.families){
      history.push('/')
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  if(!props.families){
    return <div>nothing to display</div>
  }

  return (
    <div className = 'summaryContainer'>
        <button className = 'goBackButton myButton' onClick = {() => history.goBack()}>
          Go back
        </button>
        <table className = 'summaryTable'>
          <thead>
            <tr>
              <th>Status</th>
              <th>Primary</th>
              <th>Primary + Spouse</th>
              <th>Primary + Spouse + Children</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>Confirmed</th>
              <th>{props.families.reduce(getReducer(true, ['primary']), 0)}</th>
              <th>{props.families.reduce(getReducer(true, ['primary', 'spouse']), 0)}</th>
              <th>{props.families.reduce(getReducer(true, ['primary', 'spouse', 'children'], ['primary', 'children']), 0)}</th>
            </tr>
            <tr>
              <th>Removed</th>
              <th>{props.families.reduce(getReducer(false, ['primary']), 0)}</th>
              <th>{props.families.reduce(getReducer(false, ['primary', 'spouse']), 0)}</th>
              <th>{props.families.reduce(getReducer(false, ['primary', 'spouse', 'children'], ['primary', 'children']), 0)}</th>
            </tr>
          </tbody>
        </table>
    </div>
  )
}


const compare = (array1, array2) => {

  if(array1.length !== array2.length) return false;
  array1.sort();
  array2.sort();
  for (var i = 0; i < array1.length; i++){
    if (array1[i] !== array2[i]) return false;
  }
  return true;
}

const getReducer = (selected, memberTypes, optional) => {
  const filter = (cur) => {
    if(optional){
        return compare(optional, Object.keys(cur.family)) || compare(memberTypes, Object.keys(cur.family))
    }else{
      return compare(memberTypes, Object.keys(cur.family))
    }
  }
  return (accumulator, cur) => {
    if(cur.selected === selected && filter(cur)){
      return accumulator + 1
    } else {
      return accumulator
    }
  }
}

const mapStateToProps = state => ({
  families: state.families.data
})

export default connect(mapStateToProps)(Summary);
