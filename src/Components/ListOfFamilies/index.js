import React, {useEffect, useState} from 'react'
import {connect} from 'react-redux'
import {receiveFamilies, toggleFamily} from '../../Redux/actions/families'
import {getListOfFamilies} from '../../API/getData'
import FamilyView from './FamilyView'
import FamilySelector from './FamilySelector'
import {useHistory} from  "react-router-dom"
import RingLoader from 'react-spinners/RingLoader'

import './styles/index.css'

const ListOfFamilies = (props) =>{
  const [loading, setLoading] = useState(true)
  const history = useHistory();
  const receiveData = () => {
    setLoading(true)
    getListOfFamilies().then((res) => {
      console.log(res.data)
      props.receiveFamilies(res.data)
      setLoading(false)
    }).catch((err) => {
      console.log(err)
    })
  }
  useEffect(() => {
    console.log(props.families)
    if(!props.families) {
      receiveData();
    } else {
      setLoading(false)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  if(loading){
    return (
      <div className = 'loading'>
        <RingLoader loading = {loading} size = {150} color = {'#34D7B6'}/>
      </div>
    )
  }
  return (
    <div className = 'listOfFamiliesContainer'>
      {props.families.map((family, index) => {
        return(
          <div className = 'familyContainer'  key = {family.id}>
            <FamilyView data = {family.family} selected = {family.selected}/>
            <FamilySelector selected = {family.selected} action = {() => props.toggleFamily(family.id)}/>
          </div>
        )
      })}
      <button className = 'summaryButton myButton' onClick = {() => history.push('/summary')}>Confirm</button>
    </div>
  )
}

const mapStateToProps = state => ({
  families: state.families.data
})

const mapDispatchToProps = dispatch => {
  return {
    receiveFamilies: (families) => dispatch(receiveFamilies(families)),
    toggleFamily: (familyId) => dispatch(toggleFamily(familyId))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListOfFamilies);
