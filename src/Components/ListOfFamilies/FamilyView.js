import React from 'react'

const tableGenerator = (family) => {
  return (
    Object.entries(family).map((familyMember, index) => {
    const [type, personData] = familyMember
    // primary/spouse generator
    if(type === 'primary' || type === 'spouse'){
      const [firstName, lastName] = personData.name.split(' ')
      return (
        <tr key = {index}>
          <th>{capitalizeFirstLetter(type)}</th>
          <th>{personData.ssn}</th>
          <th className = 'name'>{firstName}</th>
          <th className = 'name'>{lastName}</th>
        </tr>
      )
    } else {
      //child generator
      return(
        personData.map((child, i) => {
          const [firstName, lastName] = child.name.split(' ')
          return (
            <tr key = {index + '' + i}>
              <th>Child</th>
              <th>{child.ssn}</th>
              <th className = 'name'>{firstName}</th>
              <th className = 'name'>{lastName}</th>
            </tr>
          )
        })
      )
    }
    })
  )
}

const FamilyView = (props) =>{
  if(!props.data){
    return <div>loading2</div>
  }
  return (
    <table className = 'FamilyView'>
      <thead>
        <tr className = {props.selected ? 'selected' : 'notSelected'}>
          <th>Member type</th>
          <th>SSN</th>
          <th>First Name</th>
          <th>Last Name</th>
        </tr>
      </thead>
      <tbody>
        {tableGenerator(props.data)}
      </tbody>
    </table>
  )
}
function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export default FamilyView;
