import {
  Route,
  Switch
 } from 'react-router-dom';
import React from 'react'
import ListOfFamilies from '../Components/ListOfFamilies'
import Summary from '../Components/Summary'

const Routes = () => (
  <Switch>
      <Route exact path = '/' component = {ListOfFamilies}/>
      <Route exact path = '/summary' component = {Summary}/>
      <Route render={() => {
          return <p>Not Found</p>
        }}/>
  </Switch>
)

export default Routes;
