import React from 'react';
import {Provider} from 'react-redux'
import './App.css';
import store from './Redux/store'
import { BrowserRouter as Router} from 'react-router-dom'
import Routes from './Routes/routes.js'
function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <Routes/>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
