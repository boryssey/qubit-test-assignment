import families from './families'
import {combineReducers} from 'redux'


export default combineReducers({
  families
})
