import axios from 'axios'
const apiAddress = 'https://nioetnibs2.execute-api.us-east-1.amazonaws.com/prod'

axios.defaults.baseURL = apiAddress;

export default axios
